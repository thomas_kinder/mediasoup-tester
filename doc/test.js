const { Builder, By, Key, until, Capabilities, WebDriverWait } = require('selenium-webdriver');
const fs = require('fs')
const path = require("path");
const { verifyVideoDisplayById, getPixelSumsByIdScript } = require('kite-common/util/TestUtils')

var driver = null;
var cfg = JSON.parse(fs.readFileSync(path.resolve(__dirname, "./config.json")));


init_chrome = async () => {
    var chromeCapabilities = Capabilities.chrome();
    var chrome = require('selenium-webdriver/chrome');
    var options = new chrome.Options();
    
    console.log("init_chrome: called");
    
    options.addArguments("--use-fake-ui-for-media-stream");
    options.addArguments("--use-fake-device-for-media-stream");
    
    chromeCapabilities.set('chromeOptions', options);


    driver = await new Builder()
        .forBrowser("chrome")
        .withCapabilities(options)
        .build();
    
}

loginProctorOne = async (driver) => {
    await driver.get("http://localhost:3000");
    let input = driver.findElement(By.id('userId'));

    await driver.wait(until.elementIsVisible(input), 100);
    await input.sendKeys("0");

    let btn = driver.findElement(By.id('btnLogin'))
    await driver.wait(until.elementIsVisible(btn));
    await btn.click();
}


/**
 * Test 001 - test a single room with 2 proctors and 24 participants
 */
test_001 = async () => {
    try {

        loginProctorOne(driver);

    } catch (error) {
        console.log("test_001:: caught error = ", error);
    }
}


//_---------------------------------------------------------------

var browser_init = {
    "chrome": init_chrome,
    "firefox": () => {console.log("firefox init not defined yet")}
}

var test_func = {
    "001": test_001,
}


test_main = () => {
    browser = cfg.browser;
    testToRun = cfg.testToRun;

    //init the driver
    browser_init[browser]();

    //Run all test case defined in the config
    for (let i = 0; i < testToRun.length; i++){
        test_func[testToRun[i]]();
    }

}

test_main();







async function example() {
    // let options = {
    //     'args': [
    //         '--start-maximized',
    //         '--use-fake-ui-for-media-stream',
    //         '--allow-insecure-localhost',
    //         '--use-file-for-fake-video-capture=/home/thomas/Videos/out.y4m'
    //     ]
    // }

    var chromeCapabilities = Capabilities.chrome();
    chromeCapabilities.set('chromeOptions', options);

    var chrome = require('selenium-webdriver/chrome');
    var options = new chrome.Options();
    options.addArguments("--use-fake-ui-for-media-stream");
    options.addArguments("--use-fake-device-for-media-stream");
    // options.addArguments("--headless");
    // options.addArguments("--use-file-for-fake-video-capture=/home/thomas/Videos/out.y4m");


    console.log(options)
    let driver = await new Builder()
        .forBrowser("chrome")
        .withCapabilities(options)
        .build();

    try {

        //Prepare the browser by setting up the windows. 
        //windo handle 0 = proctor 0
        //window handle 1 = proctor 1
        //window handle 2 -> 25 participants
        for (var i = 0; i < cfg.roomSize; i++) {
            console.log("here")
            if (i == 0) {
                await driver.get("http://localhost:3000");
                let input = driver.findElement(By.id('userId'));

                await driver.wait(until.elementIsVisible(input), 100);
                await input.sendKeys("0");

                let btn = driver.findElement(By.id('btnLogin'))
                await driver.wait(until.elementIsVisible(btn));
                await btn.click();

            } else {
                await driver.executeScript('window.open(" ")');
                let winHandle = await driver.getAllWindowHandles();
                let lastHandle = winHandle[winHandle.length - 1];
                console.log(lastHandle)
                driver.switchTo().window(lastHandle);

                await driver.get("http://localhost:3000");
            }
        }

        let all = await driver.getAllWindowHandles();

        //index 0 = proctor 0, index = proctor 1
        for (var i = 0; i < all.length; i++) {
            await driver.switchTo().window(all[i]);
            let userId = await driver.findElement(By.id('userId'));
            await driver.wait(until.elementIsVisible(userId), 1000);
            await driver.wait(until.elementIsEnabled(userId ,15000));
            let off = i ; 
            
            if (i < 2){//proctor 0 / 1 can login directly
                await userId.sendKeys(off.toString(), Key.RETURN);
                
            } else {
                await userId.sendKeys(off.toString(), Key.RETURN);
                
                //check if participant video has came after 5 seconds waitint
                await driver.switchTo().window(all[0]);
                await driver.sleep(3000);
                ret = await verifyVideoDisplayById(driver, "video" + (i-2));
                console.log("Video check = ", ret);

            }

            

            // let btn = driver.findElement(By.id('btnLogin'))
            // await driver.wait(until.elementIsVisible(btn));
            // await btn.click();
        }



    } catch (error) {
        console.log(error);
    }


}

// example();
