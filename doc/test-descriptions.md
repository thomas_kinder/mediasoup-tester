# Test Descriptions:

## Test 001 - test a single room:
    - Fill one room with 2 proctors and 24 participants
        - First proctor is added first (login id = 0)
        - Once page is loaded open another window for participant with login id = 2 to join 
        - check in 1 second intervals on  proctor window if video0 element receives a stream 
            - we wait for 20s, if stream has not come after that time test is considered failed and 
            test procedure is interrupted. 
        - If check is successful we save the number of seconds it took for the stream to be established 
          properly, which gives as an indication of how fast the user was able to login and establish WebRTC
        - We also measure the frame rate of the stream which should be in a specified range. If its not in the range
        we consider the test as failed. If the value is lower than what is specified it means most likely stream is 
        not working properly. If its higher we will give only a warning as this might mean that client is not 
        configured properly, so data rate will be higher. 
        - We run this for all the 24 clients in the room. If all test pass for all the clients we will login with the 
        2nd proctor and will check if all video streams are displayed within 30 seconds of connecting to the server. 



