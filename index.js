const { Builder, By, Key, until, Capabilities, WebDriverWait } = require('selenium-webdriver');
const fs = require('fs')
const path = require("path");
const { verifyVideoDisplayById, getPixelSumsByIdScript } = require('./kite-common/util/TestUtils')

var cfg = JSON.parse(fs.readFileSync(path.resolve(__dirname, "./config.json")));


init_chrome = async () => {
    var chromeCapabilities = Capabilities.chrome();
    var chrome = require('selenium-webdriver/chrome');
    var options = new chrome.Options();

    console.log("init_chrome: called");

    options.addArguments("--use-fake-ui-for-media-stream");
    options.addArguments("--use-fake-device-for-media-stream");
    // options.addArguments("--headless");
    // options.addArguments("--no-sandbox");

    chromeCapabilities.set('chromeOptions', options);


    let driver = await new Builder()
        .forBrowser("chrome")
        .withCapabilities(options)
        .build();

    return driver;

}

loginProctorOne = async (driver, url) => {

    return await _loginUser(driver, 0, url);
}

loginProctorTwo = async (driver, url) => {
    return await _loginUser(driver, 1, url);
}

loginParticipant = async (driver, id, url) => {
    return await _loginUser(driver, id + 2, url);
}



_loginUser = async (driver, id, url) => {
    // console.log("url :::::::9999:", url);
    var handle = null;

    if (global.firstCall) {
        await driver.get(url);
        handle = driver.getWindowHandle();
        global.firstCall = false;

    } else {
        await driver.executeScript('window.open(" ")');
        let winHandle = await driver.getAllWindowHandles();
        handle = winHandle[winHandle.length - 1];

        await driver.switchTo().window(handle);
        await driver.get(url);

    }

    let input = await driver.findElement(By.id('userId'));

    await driver.wait(until.elementIsVisible(input), 100);
    await input.sendKeys(id.toString());

    let btn = driver.findElement(By.id('btnLogin'))
    await driver.wait(until.elementIsVisible(btn));
    await btn.click();

    return handle;
}

checkUserStream = async (driver, videoId) => {
    var ret;

    for (var i = 0; i < 20; i++) {
        try {

            ret = await verifyVideoDisplayById(driver, videoId);
            if (ret.result == 'video') {
                return { videoId: videoId, loopCnt: i, status: ret.result };
            }

            // await driver.sleep(500);

        } catch (err) {
            console.log(err);
        }
    }

    return { videoId: videoId, loopCnt: i, status: ret.result };

}

/**
 * Test 001 - test a single room with 2 proctors and 24 participants
 * first add one user at a time and check video than we login with 
 * proctor two and check if all videos are playing. 
 */
test_001 = async (driver, args, url) => {
    console.log("start test_001...")
 
    try {
        var userHandles = []
        var procOneHandle = null;
        var testResult = {
            test: "test_001",
            proctorOne: {},
            proctorTwo: {}
        }

        procOneHandle = await loginProctorOne(driver, url);

        //add user streams one by one and check if videos are coming
        for (let i = 0; i < 24; i++) {
            console.log("I == ", i);
            userHandles.push(await loginParticipant(driver, i, url));
            await driver.switchTo().window(procOneHandle);

            let status = await checkUserStream(driver, "video" + i);
            testResult["proctorOne"][i] = status;
            // console.log(status);
        }

        //join with the second proctor and check when all video streams have come
        //this is done in while for 30 seconds, after this if not all streams have come 
        //its a fail
        let procTwoHandle = await loginProctorTwo(driver, url);
        await driver.switchTo().window(procTwoHandle);
        var k = 0;
        var streamsProctor2 = [];
        var streamCheckDone = 0;
        while (true) {

            for (let i = 0; i < 24; i++) {
                let ret = await checkUserStream(driver, "video" + i);

                if (ret.status === "video") {
                    ret["loopCnt"] = k;
                    testResult["proctorTwo"][i] = ret;
                    streamCheckDone++;
                }
            }

            break; //for testing on the server

            // if (streamCheckDone == 24) {
            //     break;
            // }

            // // await driver.sleep(1000);

            // if (k == 30) {
            //     console.log("Second proctor test failed");
            //     break;
            // }

            k++;
        }

        console.log("Result:: ", testResult);
        driver.close();



    } catch (error) {
        console.log("test_001:: caught error = ", error);
    }
}


/**
 * test_002 : fill N rooms completely and then check the proctors 
 * if they receive the videos
 */
test_002 = async (driver, args, url) => {
    console.log("test_002:: called. args = ", args);
    var testResult = {
        test: "test_002",
        msg: {},
    }


    //create N rooms filled with two proctors and 24 participants
    for(let room = args.roomOffset * args.roomSize; room < args.noRooms; room++){
        for(let i = 0; i < 26; i++){
            await _loginUser(driver, i + (room * 26), url);
            console.log(i + (room * 26));
        }
    }


    //check if all proctors can see all the streams to ensure everything is running
    var proctors = []
    for(let i = 0; i < args.noRooms; i++){
        proctors.push( args.roomOffset * args.roomSize );
        proctors.push( args.roomOffset * args.roomSize + 1);
    }

    var win = await driver.getAllWindowHandles();

    for (let proc = 0; proc < proctors.length; proc++){
        await driver.switchTo().window(win[proc]);
        for (let i = 0; i < 24; i++) {
            let ret = await checkUserStream(driver, "video" + i);
    
            if (ret.status === "video") {
                testResult["msg"][i] = ret;
            }
        }
    }

    console.log("Result:: ", testResult);

}



//_---------------------------------------------------------------

var browser_init = {
    "chrome": init_chrome,
    "firefox": () => { console.log("firefox init not defined yet") }
}

var test_func = {
    "001": test_001,
    "002": test_002
}


test_main = async () => {
    try {
        browser = cfg.browser;
        testToRun = cfg.testToRun;
        var url = cfg.url;
        

        //init the driver
        var driver = await browser_init[browser]();

        //Run all test case defined in the config
        for (let i = 0; i < testToRun.length; i++) {
            global.firstCall = true;
            let tmp = testToRun[i].split("|");
            let name = tmp[0].trim();
            let args = {}

            if (tmp.length == 2) {
                args = JSON.parse(tmp[1].trim());
            }
            console.log("url :::::::", url);
            await test_func[name](driver, args, url);
        }

    } catch (error) {
        console.log(error);
    }
}

test_main();







async function example() {
    // let options = {
    //     'args': [
    //         '--start-maximized',
    //         '--use-fake-ui-for-media-stream',
    //         '--allow-insecure-localhost',
    //         '--use-file-for-fake-video-capture=/home/thomas/Videos/out.y4m'
    //     ]
    // }

    var chromeCapabilities = Capabilities.chrome();
    chromeCapabilities.set('chromeOptions', options);

    var chrome = require('selenium-webdriver/chrome');
    var options = new chrome.Options();
    options.addArguments("--use-fake-ui-for-media-stream");
    options.addArguments("--use-fake-device-for-media-stream");
    // options.addArguments("--headless");
    // options.addArguments("--use-file-for-fake-video-capture=/home/thomas/Videos/out.y4m");


    console.log(options)
    let driver = await new Builder()
        .forBrowser("chrome")
        .withCapabilities(options)
        .build();

    try {

        //Prepare the browser by setting up the windows. 
        //windo handle 0 = proctor 0
        //window handle 1 = proctor 1
        //window handle 2 -> 25 participants
        for (var i = cfg.roomOffset * cfg.roomSize; i < cfg.roomSize; i++) {
            console.log("here")
            if (i == 0) {
                await driver.get("http://localhost:3000");
                let input = driver.findElement(By.id('userId'));

                await driver.wait(until.elementIsVisible(input), 100);
                await input.sendKeys("0");

                let btn = driver.findElement(By.id('btnLogin'))
                await driver.wait(until.elementIsVisible(btn));
                await btn.click();

            } else {
                await driver.executeScript('window.open(" ")');
                let winHandle = await driver.getAllWindowHandles();
                let lastHandle = winHandle[winHandle.length - 1];
                console.log(lastHandle)
                driver.switchTo().window(lastHandle);

                await driver.get("http://localhost:3000");
            }
        }

        let all = await driver.getAllWindowHandles();

        //index 0 = proctor 0, index = proctor 1
        for (var i = 0; i < all.length; i++) {
            await driver.switchTo().window(all[i]);
            let userId = await driver.findElement(By.id('userId'));
            await driver.wait(until.elementIsVisible(userId), 1000);
            await driver.wait(until.elementIsEnabled(userId, 15000));
            let off = i;

            if (i < 2) {//proctor 0 / 1 can login directly
                await userId.sendKeys(off.toString(), Key.RETURN);

            } else {
                await userId.sendKeys(off.toString(), Key.RETURN);

                //check if participant video has came after 5 seconds waitint
                await driver.switchTo().window(all[0]);
                await driver.sleep(3000);
                ret = await verifyVideoDisplayById(driver, "video" + (i - 2));
                console.log("Video check = ", ret);

            }



            // let btn = driver.findElement(By.id('btnLogin'))
            // await driver.wait(until.elementIsVisible(btn));
            // await btn.click();
        }



    } catch (error) {
        console.log(error);
    }


}

// example();
